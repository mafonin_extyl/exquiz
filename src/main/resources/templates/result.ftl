<#import "macros/layout.ftl" as layout />

<@layout.header />

<div class="col-lg-12" id="content">

    <div class="col-lg-12">
        <h1>Результаты</h1>
        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Название команды</th>
                        <th>Очки</th>
                    </tr>
                </thead>
                <tbody>
                    <#list commands as command>
                        <tr>
                            <td>
                                ${command.name}
                            </td>
                            <td>
                                ${command.score}
                            </td>
                        </tr>
                    </#list>
                </tbody>
            </table>

        </div>
    </div>

</div>

<@layout.footer />
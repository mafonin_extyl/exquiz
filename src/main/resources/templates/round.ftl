<#import "macros/layout.ftl" as layout />

<@layout.header />

<div class="col-lg-12" id="content">

    <h1>Раунд ${round.getNum()} ${round.getName()}</h1>

</div>

<@layout.footer />
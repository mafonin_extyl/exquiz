<#import "../macros/admin_layout.ftl" as layout>
<@layout.header />
  <div id="main-content" class="container" style="padding-top: 20px;">
      <div class="row">
          <div class="col-lg-12">
              <h1>Раунды</h1>
              <div class="row">
                  <table class="table table-bordered">
                      <tr>
                          <th>Идентификатор</th>
                          <th>Номер</th>
                          <th>Название</th>
                          <th></th>
                      </tr>
                  <#list rounds as round>
                      <tr>
                          <td>${round.id}</td>
                          <td>${round.num}</td>
                          <td>${round.name}</td>
                          <td><a class="btn btn-info" href="/admin/rounds/${round.id}">Изменить</a></td>
                      </tr>
                  </#list>
                  </table>
                  <a href="/admin/rounds/add" class="btn btn-success">Добавить</a>
              </div>
          </div>
      </div>
  </div>
<@layout.footer />
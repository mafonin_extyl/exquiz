<#import "../macros/admin_layout.ftl" as layout>
<@layout.header />
  <div id="main-content" class="container" style="padding-top: 20px;">
      <div class="row">
          <div class="col-lg-12">
              <h1>Вопросы</h1>
              <div class="row">
                  <table class="table table-bordered">
                      <tr>
                          <th>Идентификатор</th>
                          <th>Название</th>
                          <th>Сортировка</th>
                          <th>Время на ответ</th>
                          <th></th>
                      </tr>
                  <#list questions as question>
                      <tr>
                          <td>${question.id}</td>
                          <td>${question.name}</td>
                          <td>${question.sort}</td>
                          <td><#if question.duration?? >${question.duration}</#if></td>
                          <td><a class="btn btn-info" href="/admin/questions/${question.id}">Изменить</a></td>
                      </tr>
                  </#list>
                  </table>
                  <a href="/admin/questions/add" class="btn btn-success">Добавить</a>
              </div>
          </div>
      </div>
  </div>
<@layout.footer />
<#import "../macros/admin_layout.ftl" as layout>
<@layout.header />
  <div id="main-content" class="container" style="padding-top: 20px;">
      <div class="row">
          <div class="col-lg-6">
              <div class="row">
                  <#list rounds as round>
                      <div class="col-lg-12">
                          <div class="row">
                              <div class="col-lg-6">
                                  ${round.name}
                              </div>
                              <div class="col-lg-3">
                                  <button class="btn btn-info" onclick="Application.startRound(${round.id})">Начать</button>
                              </div>
                              <div class="col-lg-12">
                                  <div class="row">
                                  <#list roundsQuestions[round.id?string] as question>
                                      <div class="col-lg-12">
                                          <div class="col-lg-8">
                                            ${question.name}
                                          </div>
                                          <div class="col-lg-2">
                                              <button class="btn btn-success" onclick="Application.presentQuestion(${question.id})">Вопрос</button>
                                          </div>
                                          <div class="col-lg-2">
                                              <button class="btn btn-success" onclick="Application.presentQuestionAnswer(${question.id})">Ответ</button>
                                          </div>
                                      </div>
                                  </#list>
                                  </div>

                              </div>
                          </div>
                      </div>
                  </#list>
              </div>
          </div>
          <div class="col-lg-6">
              <div class="row">
                  <div class="col-lg-6">
                      <button class="btn btn-info" onclick="Application.nextQuestion()">Следующий вопрос</button>
                      <button class="btn btn-info" style="margin-top: 5px;" onclick="Application.startQuestion()">Стартовать вопрос</button>
                  </div>
                  <div class="col-lg-6">
                      <div class="col-lg-12"><b>Установить состояние</b></div>
                      <div class="col-lg-12">
                          <button class="btn btn-info" onclick="Application.changeState('INIT')">Init</button>
                          <br/>
                      </div>
                      <div class="col-lg-12">
                          <button class="btn btn-info" style="margin-top: 5px;" onclick="Application.changeState('WELCOME')">Приветствие</button>
                          <br/>
                      </div>
                      <div class="col-lg-12">
                          <button class="btn btn-info" style="margin-top: 5px;" onclick="Application.changeState('WAIT')">Ожидание</button>
                          <br/>
                      </div>
                      <div class="col-lg-12">
                          <button class="btn btn-info" style="margin-top: 5px;" onclick="Application.changeState('RESULT')">Результаты</button>
                          <br/>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-lg-12" style="margin-top: 10px;">
              <div class="row">
                  <h2>Окно игроков</h2>
                  <iframe src="/" style="width: 100%; height: 500px;"></iframe>
              </div>
          </div>
      </div>
  </div>
<@layout.footer />
<#import "../macros/admin_layout.ftl" as layout>
<@layout.header />
  <div id="main-content" class="container" style="padding-top: 20px;">
      <div class="row">
          <div class="col-lg-12 text-center">
              <form action="/admin/rounds/edit/${round.id}" method="post" class="form-horizontal">
                  <div class="form-group">
                      <label class="label">Название</label>
                      <input class="form-control" type="text" name="name" value="${round.name}">
                  </div>
                  <div class="form-group">
                      <label class="label">Номер</label>
                      <input class="form-control" type="text" name="num" value="${round.num}">
                  </div>
                  <div class="form-group">
                      <a href="/admin/rounds" class="btn btn-default">Отменить</a>
                      <input class="btn btn-success" type="submit" value="Изменить">
                  </div>
              </form>
          </div>
      </div>
  </div>
<@layout.footer />
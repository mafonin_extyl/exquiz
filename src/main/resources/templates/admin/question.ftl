<#import "../macros/admin_layout.ftl" as layout>
<@layout.header />
  <div id="main-content" class="container" style="padding-top: 20px;">
      <div class="row">
          <div class="col-lg-12 text-center">
              <form action="/admin/questions/edit/${question.id}" method="post" class="form-horizontal">
                  <div class="form-group">
                      <label class="label">Название</label>
                      <input class="form-control" type="text" name="name" value="${question.name}">
                  </div>
                  <div class="form-group">
                      <label class="label">Описание</label>
                      <textarea class="form-control" name="description">
                      ${question.description}
                      </textarea>
                  </div>
                  <div class="form-group">
                      <label class="label">Правильный ответ</label>
                      <textarea class="form-control" name="correctAnswer">
                      ${question.correctAnswer}
                      </textarea>
                  </div>
                  <div class="form-group">
                      <label class="label">Раунд</label>
                      <select class="form-control" name="round">
                          <#list rounds as round>
                              <option value="${round.id}" <#if question.round?? && round.id == question.round > selected </#if> >${round.name}</option>
                          </#list>
                      </select>
                  </div>
                  <div class="form-group">
                      <label class="label">Время на ответ (сек)</label>
                      <input class="form-control" type="text" name="duration" value="<#if question.duration?? >${question.duration}</#if>">
                  </div>
                  <div class="form-group">
                      <label class="label">Сортировка</label>
                      <input class="form-control" type="text" name="sort" value="${question.sort}">
                  </div>
                  <div class="form-group">
                      <a href="/admin/questions" class="btn btn-default">Отменить</a>
                      <input class="btn btn-success" type="submit" value="Изменить">
                  </div>
              </form>
          </div>
      </div>
  </div>
<@layout.footer />
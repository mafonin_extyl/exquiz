<#import "../macros/admin_layout.ftl" as layout>
<@layout.header />
  <div id="main-content" class="container" style="padding-top: 20px;">
      <div class="row">
          <div class="col-lg-12">
              <h1>Команды</h1>
              <div class="row">
                  <table class="table table-bordered">
                      <tr>
                          <th>Идентификатор</th>
                          <th>Название</th>
                          <th>Баллов</th>
                          <th></th>
                      </tr>
                  <#list commands as command>
                      <tr>
                          <td>${command.id}</td>
                          <td>${command.name}</td>
                          <td>${command.score}</td>
                          <td><a class="btn btn-info" href="/admin/commands/${command.id}">Изменить</a></td>
                      </tr>
                  </#list>
                  </table>
                  <a href="/admin/commands/add" class="btn btn-success">Добавить</a>
              </div>
          </div>
      </div>
  </div>
<@layout.footer />
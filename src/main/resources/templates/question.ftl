<#import "macros/layout.ftl" as layout />

<@layout.header />

<div class="col-lg-12" id="content">

    <h2>ВОПРОС №${questionNum}</h2>

    <h1>${question.getName()}</h1>

    <blockquote class="blockquote text-center">
        <p>
            ${question.getDescription()}
        </p>
    </blockquote>

    <div id="countdown">
        <div id="countdown-number" data-countdown-start="${question.duration}">${question.duration}</div>
        <svg>
            <circle r="18" cx="20" cy="20"></circle>
        </svg>
    </div>
</div>

<@layout.footer />
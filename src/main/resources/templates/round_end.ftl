<#import "macros/layout.ftl" as layout />

<@layout.header />

<div class="col-lg-12" id="content">

    <h1>Раунд №${round.getNum()} завершен</h1>
    <h2>${round.getName()}</h2>

</div>

<@layout.footer />
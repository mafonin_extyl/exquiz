<#import "macros/layout.ftl" as layout />

<@layout.header />

<div class="col-lg-12" id="content">

    <h1>${question.getName()}</h1>

    <blockquote class="blockquote text-center">
        <p>
        ${question.getCorrectAnswer()}
        </p>
    </blockquote>

</div>

<@layout.footer />
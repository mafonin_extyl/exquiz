<#macro header>
    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="/static/css/app.css">

        <title>Extyl Quiz</title>
    </head>
    <body>
    <div class="col-lg-12">
        <div id="ws-connection-connected" class="alert alert-success" style="display: none;">
            Соединение с сервером установлено.
        </div>
        <div id="ws-connection-disconnected" class="alert alert-danger" style="display: none;">
            Соединение с сервером потеряно. Перезагрузите страницу.
        </div>
    </div>
    <div class="page-wrapper">

        <div class="bg"></div>
        <div class="content-wrapper">
            <div class="container" id="main-container">
            <div class="logo"><img src="/static/img/logo.svg"/></div>
            <div id="container">
</#macro>

<#macro footer>
            </div>
            </div>
        </div>

        <div class="preloader"></div>
    </div>
    <script src="/webjars/sockjs-client/1.0.2/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
    <script src="/webjars/jquery/3.1.0/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="/static/js/app.js"></script>
</body>
</html>
</#macro>

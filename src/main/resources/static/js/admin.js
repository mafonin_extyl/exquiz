var stompClient = null;

var Application = function(){

    var changeState = function(state) {
        var ChangeStateMessage = {
            state: state
        };

        stompClient.send("/app/changeState", {}, JSON.stringify(ChangeStateMessage));
    };

    var startRound = function(id) {
        $.post("/state/startRound", {roundId: id}, function (result) {});
    };

    var nextQuestion = function() {
        $.post("/state/nextQuestion", function (result) {});
    };

    var startQuestion = function() {
        $.post("/state/startQuestion", function (result) {});
    };

    var presentQuestion = function(id) {
        $.post("/state/presentQuestion", {id: id}, function (result) {});
    };

    var presentQuestionAnswer = function(id) {
        $.post("/state/presentQuestionAnswer", {id: id}, function (result) {});
    };

    return {
        changeState: changeState,
        startRound: startRound,
        nextQuestion: nextQuestion,
        startQuestion: startQuestion,
        presentQuestion: presentQuestion,
        presentQuestionAnswer: presentQuestionAnswer,
    }

}();

function connect() {
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.debug = true;
    stompClient.connect({}, function (frame) {
        setConnected(true);
    });
}

function setConnected(connected) {
    if(connected) {
        $('#ws-connection-disconnected').hide();
        $('#ws-connection-connected').fadeIn("slow");

        setTimeout(function(){
            $('#ws-connection-connected').fadeOut("slow");
        }, 1000);

    } else {
        $('#ws-connection-connected').hide();
        $('#ws-connection-disconnected').fadeIn("slow");

        setTimeout(function(){
            $('#ws-connection-disconnected').fadeOut("slow");
        }, 1000);
    }
}

$(function(){
    connect();
});
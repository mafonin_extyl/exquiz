var stompClient = null;

var Application = function(){

    var _currentState = null;
    var _countdown = 0;
    var _countdownInterval = null;

    var onChangeState = function(result) {
        var parsedObject = JSON.parse(result.body);
        _currentState = parsedObject.content;
        clearInterval(_countdownInterval);

        if(_currentState === "QUESTION_STARTED") {
            _countdown = parsedObject.countdown;
        }

        _renderByState();
    };

    var _renderByState = function() {
        switch(_currentState) {
            default:
            case "RELOAD_STATE":
            case "INIT":
                _getHTMLFromServer("/");
                break;
            case "QUESTION_STARTED":
                _startQuestion();
                break
        }
    };

    var _startQuestion = function() {
        var countDownEl = $('#countdown-number');

        countDownEl.attr('data-countdown-start', parseInt(_countdown)).html(_countdown);

        $("#countdown").find("svg > circle").css({
            "animation": "countdown " + parseInt(_countdown) + "s linear infinite forwards"
        });

        _countdownInterval = setInterval(function(){
            var currentValue = countDownEl.attr('data-countdown-start');
            var nextValue = currentValue - 1;

            countDownEl.attr('data-countdown-start', nextValue).html(nextValue);

            if(nextValue < 0) {
                clearInterval(_countdownInterval);
                return;
            }

        }, 1000);
    };

    var _getHTMLFromServer = function (url, data) {
        if(data === undefined) {
            data = {};
        }

        $('.preloader').fadeIn(function(){
            $('#container').load( "/ #container>div:first-child", data, function(){
                $('.preloader').fadeOut();
            });
        });
    };

    return {
        onChangeState: onChangeState,
    }

}();

function connect() {
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.debug = true;
    stompClient.connect({}, function (frame) {
        setConnected(true);
        stompClient.subscribe('/topic/changeState', Application.onChangeState);
    });
}

function setConnected(connected) {
    if(connected) {
        $('#ws-connection-disconnected').hide();
        $('#ws-connection-connected').fadeIn("slow");

        setTimeout(function(){
            $('#ws-connection-connected').fadeOut("slow");
        }, 1000);

    } else {
        $('#ws-connection-connected').hide();
        $('#ws-connection-disconnected').fadeIn("slow");

        setTimeout(function(){
            $('#ws-connection-disconnected').fadeOut("slow");
        }, 1000);
    }
}

$(function(){
    connect();
});
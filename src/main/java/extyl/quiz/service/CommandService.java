package extyl.quiz.service;

import extyl.quiz.dao.CommandDao;
import extyl.quiz.domain.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandService {

    @Autowired
    private CommandDao commandDao;

    public List<Command> getCommands() {
        return commandDao.findAll(Sort.by(Sort.Direction.DESC, "score"));
    }

}

package extyl.quiz.service;

import extyl.quiz.dao.QuestionDao;
import extyl.quiz.dao.RoundDao;
import extyl.quiz.domain.Question;
import extyl.quiz.domain.Round;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoundService {

    @Autowired
    private RoundDao roundDao;

    @Autowired
    private QuestionDao questionDao;

    public List<Question> getRoundQuestions(Long roundId) {
        return questionDao.findAllByRoundOrderBySort(roundId);
    }

    public List<Round> getRounds() {
        return roundDao.findAll();
    }
}

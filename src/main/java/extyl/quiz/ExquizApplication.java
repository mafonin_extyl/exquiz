package extyl.quiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExquizApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExquizApplication.class, args);
	}
}

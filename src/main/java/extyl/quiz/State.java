package extyl.quiz;

import extyl.quiz.domain.Question;
import extyl.quiz.domain.Round;
import lombok.Data;
import java.util.List;

@Data
public class State {

    private State() {}

    /*
        INIT - приложение стартануло
        WELCOME - приветственное сообщение
        WAIT - ожидание
        ROUND - идет раунд
        ROUND_END - правильный ответы на вопросы
        END - матч завершен
        RESULT - таблица результатов
        PRESENT - покваз
     */
    private String globalState = "INIT";
    /*
        ROUND - показываем заставку раунда
        ROUND_END - раунд завершен
        QUESTION - показываем вопрос
        QUESTION_STARTED - обратный отсчет по вопросу
        QUESTION_ENDED - вопрос завершен
        QUESTION_ANSWER - правильный ответ
     */
    private String localState;
    private Question currentQuestion;
    private Integer nextQuestionIndex = 0;
    private Round currentRound;
    private List<Question> questions;

    public static class StateHolder {
        public static final State HOLDER_INSTANCE = new State();
    }

    public static State getInstance() {
        return StateHolder.HOLDER_INSTANCE;
    }


}

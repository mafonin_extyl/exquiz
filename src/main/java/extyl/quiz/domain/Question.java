package extyl.quiz.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private Integer duration = 60;
    private String correctAnswer;
    @Column(name = "round_id")
    private Long round;
    private Integer sort = 0;

    public Question() {
    }

}

package extyl.quiz.config;

import org.hibernate.dialect.MySQL5Dialect;

public class LocalMysqlDialect extends MySQL5Dialect {
    @Override
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8";
    }
}
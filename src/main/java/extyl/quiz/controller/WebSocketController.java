package extyl.quiz.controller;

import extyl.quiz.State;
import extyl.quiz.ws.Result;
import extyl.quiz.ws.message.ChangeStateMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    private static State state = State.getInstance();

    @MessageMapping("/changeState")
    @SendTo("/topic/changeState")
    public Result changeState(ChangeStateMessage message) {
        state.setGlobalState(message.getState());
        return new Result(message.getState());
    }

}

package extyl.quiz.controller;

import extyl.quiz.State;
import extyl.quiz.dao.CommandDao;
import extyl.quiz.dao.QuestionDao;
import extyl.quiz.dao.RoundDao;
import extyl.quiz.domain.Command;
import extyl.quiz.domain.Question;
import extyl.quiz.domain.Round;
import extyl.quiz.service.CommandService;
import extyl.quiz.service.RoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private RoundService roundService;

    @Autowired
    private CommandService commandService;

    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private RoundDao roundDao;

    @Autowired
    private CommandDao commandDao;

    @RequestMapping("/admin")
    public String admin(Model model) {
        List<Round> rounds = roundService.getRounds();

        HashMap<String, List<Question>> roundsQuestions = new HashMap<>();

        for(Round round : rounds) {
            roundsQuestions.put(round.getId().toString(), roundService.getRoundQuestions(round.getId()));
        }

        model.addAttribute("rounds", rounds);
        model.addAttribute("roundsQuestions", roundsQuestions);

        return "admin/index";
    }

    /*
        QUESTIONS
     */

    @GetMapping("admin/questions")
    public String questions(Model model) {
        List<Question> questions = questionDao.findAll();

        model.addAttribute("questions", questions);

        return "admin/questions";
    }

    @GetMapping("admin/questions/{id}")
    public String editQuestion(Model model, @PathVariable("id") Question question) {
        List<Round> rounds = roundService.getRounds();
        model.addAttribute("rounds", rounds);
        model.addAttribute("question", question);
        return "admin/question";
    }

    @PostMapping("admin/questions/edit/{id}")
    public String editQuestion(@ModelAttribute("question") Question question) {
        questionDao.save(question);
        return "redirect:/admin/questions/" + question.getId();
    }

    @GetMapping("admin/questions/add")
    public String addQuestion(Model model) {
        List<Round> rounds = roundService.getRounds();
        model.addAttribute("rounds", rounds);
        return "admin/question_add";
    }

    @PostMapping("admin/questions/add")
    public String addQuestion(@ModelAttribute("question") Question question) {
        questionDao.save(question);
        return "redirect:/admin/questions";
    }

    /*
        ROUNDS
     */

    @GetMapping("admin/rounds")
    public String rounds(Model model) {
        List<Round> rounds = roundDao.findAll();
        model.addAttribute("rounds", rounds);
        return "admin/rounds";
    }

    @GetMapping("admin/rounds/{id}")
    public String editRound(Model model, @PathVariable("id") Round round) {
        model.addAttribute("round", round);
        return "admin/round";
    }

    @PostMapping("admin/rounds/edit/{id}")
    public String editRound(@ModelAttribute("round") Round round) {
        roundDao.save(round);
        return "redirect:/admin/rounds/" + round.getId();
    }

    @GetMapping("admin/rounds/add")
    public String addRound(Model model) {
        return "admin/round_add";
    }

    @PostMapping("admin/rounds/add")
    public String addRound(@ModelAttribute("round") Round round) {
        roundDao.save(round);
        return "redirect:/admin/rounds";
    }

    /*
        COMMANDS
     */

    @GetMapping("admin/commands")
    public String commands(Model model) {
        List<Command> commands = commandDao.findAll();
        model.addAttribute("commands", commands);
        return "admin/commands";
    }

    @GetMapping("admin/commands/{id}")
    public String editCommand(Model model, @PathVariable("id") Command command) {
        model.addAttribute("command", command);
        return "admin/command";
    }

    @PostMapping("admin/commands/edit/{id}")
    public String editCommand(@ModelAttribute("command") Command command) {
        commandDao.save(command);
        return "redirect:/admin/commands/" + command.getId();
    }

    @GetMapping("admin/commands/add")
    public String addCommand(Model model) {
        return "admin/command_add";
    }

    @PostMapping("admin/commands/add")
    public String addCommand(@ModelAttribute("round") Command command) {
        commandDao.save(command);
        return "redirect:/admin/commands";
    }

    /*

     */

    @RequestMapping("/")
    public String index(Model model) {
        State state = State.getInstance();
        String globalState = state.getGlobalState();
        String localState = state.getLocalState();

        model.addAttribute("state", state.getGlobalState());

        if(globalState.equals("ROUND")) {
            if(localState.equals("ROUND")) {
                model.addAttribute("round", state.getCurrentRound());
                return "round";
            }

            if(localState.equals("QUESTION")) {
                Integer questionNum = state.getNextQuestionIndex();
                if(questionNum == 0) {
                    questionNum = 1;
                }

                model.addAttribute("questionNum", questionNum);
                model.addAttribute("question", state.getCurrentQuestion());
                return "question";
            }

            if(localState.equals("QUESTION_ENDED")) {
                model.addAttribute("question", state.getCurrentQuestion());
                return "question_end";
            }

            if(localState.equals("ROUND_END")) {
                model.addAttribute("round", state.getCurrentRound());
                return "round_end";
            }
        }

        if(globalState.equals("PRESENT")) {
            if(localState.equals("QUESTION")) {
                model.addAttribute("question", state.getCurrentQuestion());
                return "question_present";
            }

            if(localState.equals("QUESTION_ANSWER")) {
                model.addAttribute("question", state.getCurrentQuestion());
                return "question_answer";
            }
        }

        if(globalState.equals("WELCOME")) {
            return "welcome";
        }

        if(globalState.equals("WAIT")) {
            return "wait";
        }

        if(globalState.equals("END")) {
            return "end";
        }

        if(globalState.equals("RESULT")) {
            List<Command> commands = commandService.getCommands();
            model.addAttribute("commands", commands);
            return "result";
        }

        return "index";
    }
}

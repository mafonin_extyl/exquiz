package extyl.quiz.controller;

import extyl.quiz.State;
import extyl.quiz.StateManager;
import extyl.quiz.dao.QuestionDao;
import extyl.quiz.dao.RoundDao;
import extyl.quiz.domain.Question;
import extyl.quiz.domain.Round;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("state")
public class StateController {

    private static State state = State.getInstance();

    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private RoundDao roundDao;

    @Autowired
    private StateManager stateManager;

    @PostMapping("set_question")
    public String setQuestion(@RequestParam Long id) {
        Question question = questionDao.findById(id).get();
        state.setCurrentQuestion(question);
        return "ok";
    }

    @PostMapping("set_round")
    public String setRound(@RequestParam Long id) {
        Round round = roundDao.findById(id).get();
        state.setCurrentRound(round);
        return "ok";
    }

    @PostMapping("set_state")
    public String setState(@RequestParam String newState) {
        state.setGlobalState(newState);
        return "ok";
    }

    @PostMapping("startRound")
    public String startRound(@RequestParam Long roundId) {
        Round round = roundDao.findById(roundId).get();
        stateManager.startRound(round);
        return "ok";
    }

    @PostMapping("nextQuestion")
    public String nextQuestion() {
        stateManager.nextQuestion();
        return "ok";
    }

    @PostMapping("startQuestion")
    public String startQuestion() {
        stateManager.startQuestion();
        return "ok";
    }

    @PostMapping("presentQuestion")
    public String presentQuestion(@RequestParam Long id) {
        Question question = questionDao.findById(id).get();
        stateManager.presentQuestion(question);
        return "ok";
    }

    @PostMapping("presentQuestionAnswer")
    public String presentQuestionAnswer(@RequestParam Long id) {
        Question question = questionDao.findById(id).get();
        stateManager.presentQuestionAnswer(question);
        return "ok";
    }

}

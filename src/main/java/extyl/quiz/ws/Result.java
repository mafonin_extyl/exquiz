package extyl.quiz.ws;

public class Result {

    private String content;
    private Integer countdown;

    public Result(String content) {
        this.content = content;
    }

    public Result(String content, Integer countdown) {
        this.content = content;
        this.countdown = countdown;
    }

    public String getContent() {
        return content;
    }

    public Integer getCountdown() {
        return countdown;
    }
}
package extyl.quiz;

import extyl.quiz.domain.Question;
import extyl.quiz.domain.Round;
import extyl.quiz.service.RoundService;
import extyl.quiz.ws.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StateManager {

    @Autowired
    private RoundService roundService;

    @Autowired
    private SimpMessagingTemplate stompMessage;

    private static State state = State.getInstance();

    private void broadcastStateChanged(Result data) {
        stompMessage.convertAndSend("/topic/changeState", data);
    }

    public void startRound(Round round) {
        List<Question> roundQuestions = roundService.getRoundQuestions(round.getId());

        state.setGlobalState("ROUND");
        state.setLocalState("ROUND");
        state.setCurrentRound(round);
        state.setNextQuestionIndex(0);
        state.setQuestions(roundQuestions);

        broadcastStateChanged(new Result("RELOAD_STATE"));
    }

    public void nextQuestion() {
        Integer nextQuestionIndex = state.getNextQuestionIndex();
        List<Question> roundQuestions = state.getQuestions();

        try {
            Question question = roundQuestions.get(nextQuestionIndex);
            state.setNextQuestionIndex(++nextQuestionIndex);
            state.setLocalState("QUESTION");
            state.setCurrentQuestion(question);
        } catch (IndexOutOfBoundsException e) {
            state.setLocalState("ROUND_END");
        } finally {
            broadcastStateChanged(new Result("RELOAD_STATE"));
        }
    }

    public void startQuestion() {
        Question currentQuestion = state.getCurrentQuestion();
        broadcastStateChanged(new Result("QUESTION_STARTED", currentQuestion.getDuration()));
        try {
            Thread.sleep(currentQuestion.getDuration() * 1000);
            state.setLocalState("QUESTION_ENDED");
            broadcastStateChanged(new Result("QUESTION_ENDED"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void presentQuestion(Question question) {
        state.setCurrentQuestion(question);
        state.setGlobalState("PRESENT");
        state.setLocalState("QUESTION");

        broadcastStateChanged(new Result("RELOAD_STATE"));
    }

    public void presentQuestionAnswer(Question question) {
        state.setCurrentQuestion(question);
        state.setGlobalState("PRESENT");
        state.setLocalState("QUESTION_ANSWER");

        broadcastStateChanged(new Result("RELOAD_STATE"));
    }
}

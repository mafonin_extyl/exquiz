package extyl.quiz.dao;

import extyl.quiz.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuestionDao extends JpaRepository<Question, Long> {

    List<Question> findAllByRoundOrderBySort(Long roundId);

}

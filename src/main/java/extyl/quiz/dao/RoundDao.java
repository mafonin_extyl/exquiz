package extyl.quiz.dao;

import extyl.quiz.domain.Round;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoundDao extends JpaRepository<Round, Long> {
}

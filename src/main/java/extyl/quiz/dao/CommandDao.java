package extyl.quiz.dao;

import extyl.quiz.domain.Command;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandDao extends JpaRepository<Command, Long> {
}
